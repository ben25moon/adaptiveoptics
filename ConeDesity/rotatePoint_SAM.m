function rotatedPoint = rotatePoint_SAM(x, y , theta)
% Create rotation matrix
R = [cosd(theta) -sind(theta); sind(theta) cosd(theta)];

% Rotate your point(s)
point = [x y]'; 
rotatedPoint = R.*point;

end