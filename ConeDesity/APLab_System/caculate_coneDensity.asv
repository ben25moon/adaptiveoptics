

%This script takes in cone locations and then caculates the voronoi cell so
%that only one cone is in a given cell. Then the average density will be
%caculated in pixels by
%By SJ June 8, 2022

%Reinger data has a separate script called ProcessReingerData
%C:\Users\ruccilab\Documents\AO\coneDensity\ReingerConeDensity

%%% Reasoning for flipping the cone coordinates.
%the manual cone counting program will out put a text file and this text
%file will have x being in the second collumn and y being in the first
%collumn. to correct it it we just switch the columns after loading in the
%data.

%after the swtiching the the cones, the orgin is in the top left corner of


%%% OUTPUT
% avgAreaPerCone - a matrix the size of the image. In each cell, there is a
% number that corresponds to the amount of pixels one voronoi cell takes up
% (one Voronoi cell is the area of one cone cell that doesn't have any other cone cells near by)
% this output is designed to be turned into a heat map and plotted ontop of
% the cone cells. Since the retinal cone mosaic is an imported image and
% has its origin at the top left, the matrix is left with the inerior
% retina being in the first row, and the superior retina in the last row of
% the matrix (if it is a 500x 600 matrix the superior retina would be in
% row 600).

%%% CAUTION
% if you want to plot the avgAreaPerCone just as a heatmap you need to use the
% avgAreaPerCone_ForSTAND_ALONE_PLOTTING variable (same as avgAreaPerCone, but it is flipped right side up)






%%
function caculate_coneDensity(DataFilePath,ImageName,AplabData)
warning(['The main output of this function (avgAreaPerCone) is a matrix that is designed to be plotted onto the retinal cone mosaic...' ...
    'If you want to plot just the cone density by itself the data must be fliped up-down (udflip()) or use the other output of this function ' ...
    '(avgAreaPerCone_ForSTAND_ALONE_PLOTTING)'])
% roorda -are cone locations from roorda's gui, yes = 1
%run either Reineger data or script that gets the cone coordinates
Imagefile = [ImageName '.tif'];
%load image, and then load the cone locations.
pname_1 = DataFilePath;
fname_1 = Imagefile;

if AplabData
    coneimage = imread([DataFilePath ImageName '/' Imagefile]);
    refImage = [size(coneimage,1) size(coneimage,2)];
    fid = fopen([DataFilePath ImageName '/' ImageName '.txt'],'r');
    cone_data = textscan(fid, '%f %f');
    ConeCoord = [cone_data{2} cone_data{1}]; %NOTE the inversion (see above)
else
    load([DataFilePath ImageName]);
    refImage_OG =refImage;
    coneimage = refImage;

    ConeCoord = cone_data;
end




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% plot the cone coordinates on the retinal cone image (before and after rotation)
figure;
colormap('gray');
imshow(coneimage)% Imshow will plot the figure in correct orientation
hold on;
plot(ConeCoord(:,1),ConeCoord(:,2), '.r')

% extensions = {'eps','fig','png'};
% for k = 1:length(extensions)
%     saveas(gcf, sprintf('%s_rotation',[DataFilePath(1:end-14) 'Vornoi_Images/' ImageName(1:end-4)]), extensions{k})
% end

%%%
figure;
colormap('gray');
imshow(coneimage)% Imshow will plot the figure in correct orientation
hold on;
DT = delaunayTriangulation(ConeCoord); %triangulate an area with cones a verticies
[V,r] = voronoiDiagram(DT);%calculates the voronoi regions (3-7 verticies) based off the triangulateion
Xpoints = DT.Points(:,1); %get the cone coordiantes horizontal
Ypoints = DT.Points(:,2);%get the cone coordiantes Vertical

%visualize the delauanyTriangulation on top of the retinal cone image
hold on;
gg = triplot(DT);
hold on ;
voronoi(Xpoints,Ypoints,DT.ConnectivityList);% plot the voronoi cells (cones at center)


%%%
%Another plot to verify that the voronoi cells are on top of the cones.
figure;
colormap('gray');
imshow(coneimage)% Imshow will plot the figure in correct orientation
hold on;
voronoi(Xpoints,Ypoints,DT.ConnectivityList);% plot the voronoi cells (cones at center)
legend(sprintf('%s',Imagefile ))

% extensions = {'eps','fig','png'};
% for k = 1:length(extensions)
% saveas(gcf, sprintf('%s_Voronoi',[DataFilePath(1:end-14) 'Vornoi_Images/' ImageName(1:end-4)]), extensions{k})
% end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%http://www.qhull.org/download/  ==>Just use a reference
[v,c] = voronoin(DT.Points); %list the voronoi cells vertices and its regions


xVorCell = v(:,1);% horizontal cordinates for voronoi cell
yVorCell = v(:,2);% vertical cordinates for voronoi cell

if ~AplabData
    CC_out = find(ConeCoord > 712);%get the indexes of cones corrdinates that are greater than 712
    ConeCoord(CC_out,:) = [];

    minCC = floor(min(ConeCoord));
    if minCC< 0
        minCC = 0;
    end
    maxCC = round(max(ConeCoord),0);
    if maxCC >712
        maxCC  = 712;
    end

    refImage = refImage_OG(minCC(1):maxCC(1), minCC(2):maxCC(2));

    refImage = [size(refImage,1), size(refImage,2)];
end


%
%
% xVorCell_Deg = v(:,1);
% yVorCell = v(:,2);
% %grabing the points in v from indicies c
% %gets the x and y cordinates for each vertice of the voronoi cell
% VoroniCoor = cellfun(@(z) [xVorCell(z(:)) yVorCell(z(:))], c(:), 'UniformOutput', false);


%for each pixel get the 150 nearest cone cells and caculate the average for
%each 150 cones and then summ the average up and divide by 150 cells
%the input of this loop should be pixels and then you get pixels squared
%per 150 cones.

for nPixR = 1:refImage(1)
    for nPixC = 1:refImage(2)
        %DT.points puts the collum of image as x in the first row, and row in the second. 
        idx = knnsearch(DT.Points,[nPixC,nPixR] , 'k', 150);
        nn_Coord = c(idx);
        %gets the coordinates for each vertice
        nn_VoroniCoor = cellfun(@(z) [xVorCell(z(:)) yVorCell(z(:))], nn_Coord, 'UniformOutput', false);

        for nn = 1: length(nn_VoroniCoor)
            %caculates the area (pix^2)
            areaVoroniCell(nn) = polyarea(nn_VoroniCoor{nn}(:,1), nn_VoroniCoor{nn}(:,2));
        end
        %sum the areas of 150 cells then divide by the number of cells
        %output units are pixels^2/cones.
        avgAreaPerCone(nPixR,nPixC) = nanmean(areaVoroniCell);
        sprintf('(%i, %i)',nPixR,nPixC)
    end

    %since the voronoi cells are flipped over the horizontal axis
    % (see the saved image we need to flip the avgAreaPerCone before saving it).
    avgAreaPerCone_flipUD = flipud(avgAreaPerCone);
    
    imageFlipDate = datetime(2022,08,03); %changed hardware in AO system so Superior retina up.
    %     raiseTableDate = datetime(2022,08,20); %raised table and changed 940nm collumated to 680nm.
    sessDate = datetime(str2double(ImageName(end -9:end-6)),str2double(ImageName(end -4:end-3)),str2double(ImageName(end -1:end)));
    
    if AplabData
        
        if sessDate<imageFlipDate
            Superior_MaxY = avgAreaPerCone; %superior retina at bottom of image, therefore superior retina is at the maximum Y value
            Inferior_MaxY = avgAreaPerCone_flipUD;% flip so,superior retina at top of image, therefore inferior retina is at the maximum Y value
            
            avgAreaPerCone = Inferior_MaxY;
            avgAreaPerCone_flipUD = Superior_MaxY;
            
            
            save(sprintf('%s%s/avgAreaPerCone_temp_%s.mat',DataFilePath,ImageName,ImageName),'avgAreaPerCone', 'avgAreaPerCone_flipUD','Inferior_MaxY')
            
        else
            Inferior_MaxY = avgAreaPerCone; %superior retina at bottom of image, therefore superior retina is at the maximum Y value
            Superior_MaxY = avgAreaPerCone_flipUD;% flip so,superior retina at top of image, therefore inferior retina is at the maximum Y value
            supRet_topImage = avgAreaPerCone;
            avgAreaPerCone = Inferior_MaxY;
            avgAreaPerCone_flipUD = Superior_MaxY;
            
%             avgAreaPerCone_supZeroY = avgAreaPerCone;%superior retina at top of image, therefore starts when Y is 1.
            save(sprintf('%s%s/avgAreaPerCone_temp_%s.mat',DataFilePath,ImageName,ImageName),'avgAreaPerCone', 'avgAreaPerCone_flipUD','avgAreaPerCone_supZeroY')
        end
        
        %takes a long time, so save the dataas you go along.
        
    else
        save(sprintf('%s/AvgConesPerPix/avgAreaPerCone_temp_%s',DataFilePath(1:end-15),ImageName),'avgAreaPerCone', 'avgAreaPerCone_flipUD')
    end
end

if AplabData
   save(sprintf('%s%s/avgAreaPerCone_%s.mat',DataFilePath,ImageName,ImageName),'avgAreaPerCone', 'avgAreaPerCone_flipUD')
else
    save(sprintf('%s/AvgConesPerPix/avgAreaPerCone_temp_%s',DataFilePath(1:end-15),ImageName),'avgAreaPerCone', 'avgAreaPerCone_flipUD')
end





