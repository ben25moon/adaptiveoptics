
%%
%% Figures and analysis of Cone Density

function plot_coneDensity(RootPath,DataFileName,PRLFileName,SavePath,AplabData,arrayIndex)
%EyeCandyImageInverted = datetime(2022,07,01, 'Format','ddMMMyyyy');


CDC_percentile = 10;


if AplabData
    % Load avgeAreaPerPix
    load([RootPath DataFileName '/avgAreaPerCone_' DataFileName '.mat'])
    % Load PRL file
    load([RootPath DataFileName '/' PRLFileName])
    
    pixPerdeg = 512;
    
    if exist('PRLx','var') == 1
        size(avgAreaPerCone)
        
        PRL_X = round(PRLx,0);
        PRL_Y = round(size(avgAreaPerCone,1) - PRLy,0);
    elseif exist('PRL_all','var') == 1
        PRL_X = round(PRL_all(1),0);
        PRL_Y = round(size(avgAreaPerCone,1) - PRL_all(2),0);
    else
        %         PRL_X = round(PRL_x,0);
        %         PRL_Y = round(size(avgAreaPerCone,1) - PRL_y,0);
    end
    
else
    %for Reiniger et al 2021 data
    load([RootPath DataFileName]);
    pixPerdeg = 600;
    %load refimage and PRL CDC PCD
    load(PRLFileName)
    
    PRL_X = round(PRL_x,0);
    PRL_Y = round(PRL_y,0)
    
    
end

warning('note: calculate_coneDensity Assumes that the inputted image is in the correct orientation. (If refImage has inferior retina on the top you need to flip it... see code)')

%note: calculate_coneDensity Assumes that the inputted image is in the
%correct orientation.
%-----------------------------------------------------------------------------------------------

% if AplabData
%     
%     % Since EyeCandy as of 7/1/22 produces an image with with the inferior
%     % retina on the top, flip the image up and down so we can have the superior
%     % retina on top.
%     
%     % flips so that the superior retina is on the top of the image
%     %the tag MaxY is referring to the idea that part of the retina being in
%     %the last row of the matrix. i.e. if supieror_maxY then the superior
%     %retina is in the last row while the inferior rtina is in row 1.
%     
%     %     raiseTableDate = datetime(2022,08,20); %raised table and changed 940nm collumated to 680nm.
%     imageFlipDate = datetime(2022,08,03); %changed hardware in AO system so Superior retina up.
%     sessDate = datetime(str2double(DataFileName(end -9:end-6)),str2double(DataFileName(end -4:end-3)),str2double(DataFileName(end -1:end)));
%     
%     
%     if sessDate<imageFlipDate
%         
%         Superior_MaxY = avgAreaPerCone; % Use when plotting origin is top bottom on y axis: avgAreaPerCone is a matrix that has inferior saved up, but since eye candy has inferior up use this matrix for proper orientation (for stand alone heatmap)
%         Inferior_MaxY = avgAreaPerCone_flipUD;%ForSTAND_ALONE_PLOTTING; % Use when plotting origin is top left on y axis: a matrix that has superior saved up  but since eye candy has inferior up use this matrix to plot on top of the reintal cone mosaic
%         
%         avgAreaPerCone = Inferior_MaxY;
%         avgAreaPerCone_flipUD = Superior_MaxY;
%         
%     else
%         avgAreaPerCone = supRet_topImage;
%         
%         
%     end
%     
%     refImage_OG = imread([RootPath DataFileName '/' DataFileName '.tif']);
%     refImage = flipud(refImage_OG);
% else
%     if exist('avgAreaPerCone_ForSTAND_ALONE_PLOTTING','var') == 1
%         avgAreaPerCone_flipUD = avgAreaPerCone_ForSTAND_ALONE_PLOTTING;
%     end
% end
% %-----------------------------------------------------------------------------------------------
% 
% 
% 
% 
% %%% Get avgerage area per cone and convert the pixels to degrees.
% CD_conePerPixsq = 1./avgAreaPerCone;
% CD_conePerDegsq= CD_conePerPixsq * (pixPerdeg).^2;
% CD_conePerDegsq(isinf(CD_conePerDegsq)) = 0;

%%% Get Peak Cone Density
%getting the maximum cones to compare to reinigers peak cone densitys
[PCD  PCD_index]= max(max(CD_conePerDegsq));
big_idx = find(CD_conePerDegsq == PCD );
[oursPCD_y,oursPCD_x] = ind2sub(size(CD_conePerDegsq),big_idx);

APC_Prctile = prctile(avgAreaPerCone,CDC_percentile,1);
threshCDC = prctile(APC_Prctile,CDC_percentile,2);

APC = avgAreaPerCone ;

APC(APC<threshCDC)=1;
APC(APC>=threshCDC)=0;

measurements = regionprops(APC, refImage, 'WeightedCentroid');
centerOfMass = measurements.WeightedCentroid;
oursCDC_x = centerOfMass(1);
oursCDC_y = centerOfMass(2);

% if AplabData
figure;imshow(APC);
hold on;
plot(oursCDC_x, oursCDC_y, 'r*', 'LineWidth', 2, 'MarkerSize', 16);
% end


%https://www.mathworks.com/help/thingspeak/create-heatmap-overlay-image.html
figure;
image(refImage);
hold on
OverlayImage = imagesc(CD_conePerDegsq,[min(CD_conePerDegsq(:)) max(CD_conePerDegsq(:))]);
%set(gca,'xticklabel',xt,'yticklabel',yt)

% xticklabels([1:size(CD_conePerDegsq,2)/6:size(CD_conePerDegsq,2)]/pixPerdeg)
hold on
alpha(0.3)
colorbar;
hold on

if AplabData
    plot(oursPCD_x,oursPCD_y,'^k'); hold on;
    plot(PRL_X,PRL_Y,'or'); hold on;
    plot(oursCDC_x,oursCDC_y,'*b'); hold on;
    %     xlim([PRL_X-300 PRL_X + 300])
    %     ylim([PRL_Y-300 PRL_Y + 300])
    caxis([0 12000])
    legend(sprintf('%s'),DataFileName)
    
    text(PRL_X-50, PRL_Y + 250,'Inferior Retina')
    text(PRL_X-50, PRL_Y - 250,'Superior Retina')
    text(PRL_X + 200, PRL_Y, 'Nasal Retina')
    text(PRL_X - 300, PRL_Y, 'Temporal Retina')
    
    ws = (10/60)*pixPerdeg;    % 10 arcmin
    szI = size(refImage);
    hs = 20;
    px = (PRL_X - 300)+20;                % position in x
    py =(PRL_Y + 300) - 20 - hs;   % position in y
    r = rectangle('Position',[px,py,ws,hs]);
    r.FaceColor = [1 1 1];
    t = text(px,py-1.5*hs,'10 arcmin');
    t.Color = [1 1 1];
    
    %     saveas(gcf,sprintf('%sOverlayed_HeatMap_%s',SavePath, DataFileName), 'fig')
    %     saveas(gcf,sprintf('%sOverlayed_HeatMap_%s',SavePath,DataFileName), 'png')
    %     saveas(gcf,sprintf('%s%s/Overlayed_HeatMap_%s',RootPath,DataFileName, DataFileName), 'fig')
    %     saveas(gcf,sprintf('%s%s/Overlayed_HeatMap_%s',RootPath,DataFileName,DataFileName), 'png')
else
    
    hold on;
    plot(CDC_x,CDC_y,'.r', 'MarkerSize', 5); hold on;% I flipped this because the point was the max max of the matrix, thus needed to be rotated to be ontop of an image
    plot(PCD_x,PCD_y,'^k', 'MarkerSize', 5); hold on;
    plot(PRL_x,PRL_y,'sk', 'MarkerSize', 5); hold on;
    plot(oursPCD_x,oursPCD_y,'g.', 'MarkerSize', 20); hold on;
    
end

hozrPCD_col = CD_conePerDegsq(oursPCD_y - 5: oursPCD_y + 5,:);
vertPCD_row = CD_conePerDegsq(:,oursPCD_x-5:oursPCD_x + 5)';
%convert the pixels to degrees (base off image)
pixX_PCDtrimed = [1:length(vertPCD_row)];
XaxisPCD_DEG = pixX_PCDtrimed/pixPerdeg;
pixY_PCDtrimed = [1:length(hozrPCD_col)];
YaxisPCD_DEG = pixY_PCDtrimed/pixPerdeg;
normPCD_Xaxis_deg = XaxisPCD_DEG - (oursPCD_y/pixPerdeg);
normPCD_Yaxis_deg = YaxisPCD_DEG - (oursPCD_x /pixPerdeg);

hozrPRL_col = CD_conePerDegsq(PRL_Y- 5: PRL_Y + 5,:);
vertPRL_row = CD_conePerDegsq(:,PRL_X- 5 : PRL_X + 5)';
%convert the pixels to degrees (base off image)
pixX_PRLtrimed = [1:length(vertPRL_row)];
XaxisPRL_DEG = pixX_PRLtrimed/pixPerdeg;
pixY_PRLtrimed = [1:length(hozrPRL_col)];
YaxisPRL_DEG = pixY_PRLtrimed/pixPerdeg;

normPRL_Xaxis_deg = XaxisPRL_DEG - (PRL_Y/pixPerdeg);
normPRL_Yaxis_deg = YaxisPRL_DEG - (PRL_X/pixPerdeg);

figure;
% plot(normPCD_Yaxis_deg,mean(hozrPCD_col), 'r');%horizontal meridian
hold on;
plot(normPCD_Xaxis_deg*60, mean(vertPCD_row), 'k'); %vertical meridian
hold on;
plot(normPRL_Xaxis_deg*60,mean(vertPRL_row), 'c');
legend('PCD', 'PRL')

text(-25,15000,'Superior Retina')
text(15,15000,'Inferior Retina')

title('Cone Density centered around PCD')
xlabel('Eccentricity (arcmin)')
ylabel('Cone Density (cones/degree^2)')
ylim([6000, 18000])

%% general information:

PRL_CD = CD_conePerDegsq(PRL_Y,PRL_X);
CDC_CD = CD_conePerDegsq(round(oursCDC_y,0),round(oursCDC_x,0));

%euclidian distance with pix to arcminute converstion.
PRL_PCDdist = (sqrt((PRL_X - oursPCD_x)^2 + (PRL_Y - oursPCD_y)^2))/(pixPerdeg/60);
PRL_CDCdist = (sqrt((PRL_X - oursCDC_x)^2 + (PRL_Y - oursCDC_y)^2))/(pixPerdeg/60);

PCD_CDCdist = (sqrt((oursPCD_x - oursCDC_x)^2 + (oursPCD_y - oursCDC_y)^2))/(pixPerdeg/60);
SubName = string(DataFileName);
T = table(SubName,PCD, oursPCD_x, oursPCD_y, CDC_CD,round(oursCDC_x,0),round(oursCDC_y,0),PRL_CD,PRL_X,PRL_Y, ...
    PRL_PCDdist,PRL_CDCdist,PCD_CDCdist,...
    'VariableNames', {'Subject','PCD ConeDensity','PCD X', 'PCD Y', 'CDC ConeDensity','CDC X','CDC Y', 'PRL ConeDensity','PRL X','PRL Y'...
    'PRL-PCD (arc)', 'PRL - CDC(arc)', 'PCD - CDC(arc)'});

if AplabData
    save(sprintf('%sXC/%s_CD_data.mat',RootPath,SubName), 'SubName','refImage','avgAreaPerCone' ,'CD_conePerDegsq','PCD', 'oursPCD_x', 'oursPCD_y', 'CDC_CD',...
        'oursCDC_x','oursCDC_y','PRL_CD','PRL_X','PRL_Y', ...
        'PRL_PCDdist','PRL_CDCdist','PCD_CDCdist')
else
    
    save(sprintf('%sXC/%s_CD_data.mat',RootPath(1:end-23),DataFileName(16:end-4)), 'SubName','refImage','avgAreaPerCone' ,'CD_conePerDegsq','PCD', 'oursPCD_x', 'oursPCD_y', 'CDC_CD',...
        'oursCDC_x','oursCDC_y','PRL_CD','PRL_X','PRL_Y', ...
        'PRL_PCDdist','PRL_CDCdist','PCD_CDCdist')
end
% if AplabData
% fprintf('\n\nPeak Cone density: X = %i; Y = %i;\n', oursPCD_x, oursPCD_y)
% fprintf('Cone Density Centroid: X = %i; Y = %i;\n', round(oursCDC_x,0), round(oursCDC_y,0))
% fprintf('Preferred Locus of Fixation: X = %i; Y = %i;\n', PRL_X, PRL_Y)
%
%
% T = table(string(DataFileName),PCD, oursPCD_x, oursPCD_y, CDC_CD,round(oursCDC_x,0),round(oursCDC_y,0),PRL_CD,PRL_X,PRL_Y, ...
%     PRL_PCDdist,PRL_CDCdist,PCD_CDCdist,...
%     'VariableNames', {'Subject','PCD ConeDensity','PCD X', 'PCD Y', 'CDC ConeDensity','CDC X','CDC Y', 'PRL ConeDensity','PRL X','PRL Y'...
%     'PRL-PCD (arc)', 'PRL - CDC(arc)', 'PCD - CDC(arc)'});
% % Write data to text file
% writetable(T,[SavePath DataFileName '_PCD_CDC_PRL_locs.txt'],'Delimiter','\t','WriteRowNames',true);
% writetable(T,[RootPath  DataFileName '/' DataFileName '_PCD_CDC_PRL_locs.txt'],'Delimiter','\t','WriteRowNames',true);
%
% filename = 'C:\Users\sjenks\Box\APLab-Projects\AO\Data_SummarizedSubject\SummarizedData.xlsx';
% % filename ='C:\Users\sjenks\Box\APLab-Projects\AO\ConeDensity\Test_matlabUpdate.xlsx';
% writetable(T,filename,'Sheet',1,'Range',sprintf('H%i',2+arrayIndex),'WriteVariableNames',0)
%
%
% arcEccen = [0, 10, 15 20 25];% in arc min
% arcradii = 2.5;%half the length of box in arc minutes
% [ConeDensity_ecc,tableEccen_PRL,tableEccen_CDC] = EccenConeDensity(CD_conePerDegsq,arcEccen,arcradii,pixPerdeg,PRL_X,PRL_Y, oursCDC_x, oursCDC_y)
%
% tableEccen_PRL.Properties.DimensionNames{1} = DataFileName;
% tableEccen_CDC.Properties.DimensionNames{1} = DataFileName;
% if arrayIndex == 1
%     index = 0;
% else
%     index = (arrayIndex-1);
% end
% writetable(tableEccen_PRL,filename,'Sheet',2,'Range',sprintf('B%i',(index * 6) + 3 ),'WriteVariableNames',1,'WriteRowNames',true)
% writetable(tableEccen_CDC,filename,'Sheet',2,'Range',sprintf('I%i',(index *6)+3),'WriteVariableNames',1,'WriteRowNames',true)
end





%%




