
clear;clc;close all;

%%% Change this path to where the cone_desnity folder is located (don't include 'cone_density' in the root path)
RootPath_Reiniger = 'C:/Users/sjenks/Desktop/research/rochester/AO/adaptiveoptics/ConeDesity/ReinigerConeDensity/';
RootPath = 'C:/Users/sjenks/Desktop/research/rochester/AO/adaptiveoptics/ConeDesity/APLab_System/data/';

%----------------------------------------------------------------------
%subject info
%Z055 right eye March 1,2022
Data(1).DataFileName    = 'Z055_R_2022_03_01';
Data(1).PRLFileName     = 'Z055_PRL_analysis_2022_03_01.mat';
Data(1).TomeSavePath    = 'Z:/Adaptive_Optics/APLab_System/Z055_2022_03_01/ConeDensity/';

%Z160 right eye March 1,2022
Data(2).DataFileName    = 'Z160_R_2022_03_01';
Data(2).PRLFileName     = 'Z160_PRL_analysis_2022_03_01.mat';
Data(2).TomeSavePath    = 'Z:/Adaptive_Optics/APLab_System/Z160_2022_03_01/ConeDensity/';

%Z160 right eye April 26, 2022
Data(3).DataFileName    = 'Z160_R_2022_04_26';
Data(3).PRLFileName     = 'Z160_PRL_analysis_2022_04_26.mat';
Data(3).TomeSavePath    = 'Z:/Adaptive_Optics/APLab_System/Z160_2022_04_26/ConeDensity/';

%Z190 right eye June 3, 2022
Data(4).DataFileName    = 'Z190_R_2022_06_03';
Data(4).PRLFileName     = 'Z190_840nm_PRL_analysis_2022_06_03.mat';%840 was the imaging wave, but the file is referring to what they were fixating on.. the 680 was just the center of the red box.
Data(4).TomeSavePath    = 'Z:/Adaptive_Optics/APLab_System/Z190_2022_06_03/ConeDensity/';

%Z024 right eye July 28, 2022
Data(5).DataFileName    = 'Z024_R_2022_07_28';
Data(5).PRLFileName     = 'Z024_PRL_analysis_2022_07_28.mat';%840 was the imaging wave, but the file is referring to what they were fixating on.. the 680 was just the center of the red box.
Data(5).TomeSavePath    = 'Z:/Adaptive_Optics/APLab_System/Z024_2022_07_28/ConeDensity/';

%Z151 right eye July 29, 2022
Data(6).DataFileName    = 'Z151_R_2022_07_29';
Data(6).PRLFileName     = 'Z151_PRL_analysis_2022_07_29.mat';%840 was the imaging wave, but the file is referring to what they were fixating on.. the 680 was just the center of the red box.
Data(6).TomeSavePath    = 'Z:/Adaptive_Optics/APLab_System/Z151_2022_07_29/ConeDensity/';


AplabData = 1;
%Process Cone Density?
processCD = 1;


if ~AplabData
DataFilePath = sprintf('%sConeLocations/',RootPath_Reiniger); 
LIST = dir(DataFilePath);
LIST = LIST(3:end);
    for nfiles = 1:length(LIST)
        load([DataFilePath LIST(nfiles).name]);
        DataFileName = LIST(nfiles).name;
        ImageName = imagename;
        SaveFigPath = sprintf('%sConeDesity/ReinigerConeDensity/Figures/',RootPath_Reiniger);
        if processCD
            caculate_coneDensity(DataFilePath, DataFileName, AplabData);
            close all;
            sprintf('finished with %s',ImageName);
        else
            CDPath = sprintf('%sAvgConesPerPix/712X712/',RootPath_Reiniger); 
            CDList = dir(CDPath);
%             CDList = CDList(4:end-4);
            CDList = CDList(3:end);
            CDName = CDList(nfiles).name;
            CDImage = [DataFilePath LIST(nfiles).name];
            
%             plot_coneDensity(RootPath, DataFileName,Data(ii).PRLFileName,Data(ii).TomeSavePath,AplabData)
%             
            plot_coneDensity(CDPath,CDName,CDImage,SaveFigPath,AplabData);
            close all;
        end

    end
else
    %Plot the orientation of the AO system images as of 7/22
    OrientSchem = imread([RootPath 'orientationOfVideoFrames_2022-06-22.png']);
    figure;
    imshow(OrientSchem);
    for ii = 1:length(Data)
        if processCD
            
            caculate_coneDensity(RootPath,Data(ii).DataFileName,AplabData)
%             caculate_coneDensity(Data(ii).DataFilePath, Data(ii).ImageName, Data(ii).ConeLocName,Data(ii).SaveVoroniFigPath,AplabData);
            close all;
            sprintf('finished with %s', Data(ii).ImageName)
        end
        if strcmp(Data(ii).DataFileName, '')
            warning('enter a data name')
            break;
        else
            plot_coneDensity(RootPath,Data(ii).DataFileName,Data(ii).PRLFileName,Data(ii).TomeSavePath,AplabData,ii)
%             plot_coneDensity(Data(ii).DataFilePath,Data(ii).DataFileName,Data(ii).ImageName,Data(ii).SaveFigPath,AplabData);
        end
    end
end



