
function [ConeDensity_ecc,tableEccen_PRL,tableEccen_CDC] = EccenConeDensity(CD_conePerDegsq,arcEccen,arcradii,pixPerdeg,PRL_X,PRL_Y, CDC_X, CDC_Y)
%locs = eccentricity in arc minutes
%width = how many arc area you want


width = (arcradii/60); % how big you want to take the area (square) % 2.5 arc radius (5 arc side)

ecc = round((arcEccen/60) * pixPerdeg,0); %eccentrictiy in pixels
boxLoc = round(width * pixPerdeg,0); % box area in pixels


for cc = 1:2 % prl then CDC
    center_X = [round(PRL_X,0) CDC_X];
    center_Y = [round(PRL_Y,0) CDC_Y];
    rCenter_X = center_X(cc);
    rCenter_Y =center_Y(cc);
    
    
    
    %location of boxes
    SuperiorCenter = rCenter_Y - ecc;
    InferiorCenter=  rCenter_Y + ecc;
    NasalCenter =  rCenter_X + ecc;
    TempCenter = rCenter_X - ecc;
    
    
    
    
    % %print out the schemeatic for the AO orientation
    % OrientSchem = imread('C:\Users\ruccilab\Documents\adaptiveoptics\ConeDesity\APLab_System\data\orientationOfVideoFrames_2022-06-22.png');
    % figure;
    % imshow(OrientSchem);
    
    for ii = 1: length(ecc)
        %set up the box locations
        superiorBox = CD_conePerDegsq(SuperiorCenter(ii) - boxLoc:SuperiorCenter(ii) + boxLoc,  rCenter_X - boxLoc: rCenter_X + boxLoc);
        superiorArea(ii) = mean(mean(superiorBox));
        
        inferiorBox = CD_conePerDegsq(InferiorCenter(ii) - boxLoc: InferiorCenter(ii) + boxLoc,  rCenter_X - boxLoc: rCenter_X + boxLoc);
        inferiorArea(ii) = mean(mean(inferiorBox));
        
        nasalBox = CD_conePerDegsq(rCenter_Y - boxLoc: rCenter_Y + boxLoc, NasalCenter(ii)- boxLoc : NasalCenter(ii) +boxLoc);
        nasalArea(ii) = mean(mean(nasalBox));
        
        temporalBox = CD_conePerDegsq(rCenter_Y - boxLoc: rCenter_Y + boxLoc,TempCenter(ii)- boxLoc : TempCenter(ii) + boxLoc);
        temporalArea(ii) = mean(mean(temporalBox));
    end
    
    ConeDensity_ecc{cc}=[superiorArea;inferiorArea;nasalArea;temporalArea];
    
end

tableEccen_PRL = array2table(ConeDensity_ecc{1},...
    'VariableNames',{'0' '10' '15' '20' '25'},'RowNames',{'Suprior' 'Inferior' 'Nasal' 'Temporal'});

tableEccen_CDC = array2table(ConeDensity_ecc{2},...
    'VariableNames',{'0' '10' '15' '20' '25'},'RowNames',{'Suprior' 'Inferior' 'Nasal' 'Temporal'});





